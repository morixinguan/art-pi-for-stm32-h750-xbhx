# ART-PI For STM32H750XBHx

#### 介绍
用于存放基于RT-Thread开源的ART-Pi的硬件平台的个人学习实验例程，例程主要基于Keil MDK 集成开发环境构建，会分为裸机和带RTOS的开发两个版本。

#### 使用说明

- 0.NonOS

存放的是裸机，不带操作系统的ART-PI测试验证例程

- 1.RTOS

存放的是带实时操作系统的ART-PI测试验证例程

- 2.Project

存放的是基于ART-PI的实践项目

- documents

ART-PI参考文档



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

